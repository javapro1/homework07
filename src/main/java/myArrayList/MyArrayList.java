package myArrayList;

import java.util.*;
import java.util.List;

public class MyArrayList<T> implements List<T> {

    private static final int INITIAL_SIZE = 8;
    private Object[] arr = new Object[INITIAL_SIZE];
    private int size = 0;


    public void validateIndex(int index) {
        if (index < 0 || index >= arr.length) {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public boolean add(T elem) {
        arr = Arrays.copyOf(arr, arr.length + 1);
        arr[arr.length - 1] = elem;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        for (int i = 0; i < arr.length; i++) {
            if (o.equals(arr[i])) {
                for (int j = i; j < arr.length - 1; j++) {
                    arr[j] = arr[j + 1];
                }
                arr = Arrays.copyOf(arr, arr.length - 1);
                break;
            }
        }
        return true;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        boolean modified = false;
        Iterator<?> e = iterator();
        while (e.hasNext()) {
            if (c.contains(e.next())) {
                modified = true;
            }
        }
        return modified;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for (T elem : c) {
            add(elem);
        }
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        validateIndex(index);
        T arr2[] = (T[]) Arrays.copyOf(arr, arr.length + c.size());
        System.arraycopy(arr2, 0, arr2, 0, index);
        System.arraycopy(arr2, index, arr2, index + c.size(), arr.length - index);
        System.arraycopy(c, 0, arr2, index, c.size());
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean modified = false;
        Iterator<?> e = iterator();
        while (e.hasNext()) {
            if (c.contains(e.next())) {
                e.remove();
                modified = true;
            }
        }
        return modified;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean modified = false;
        Iterator<?> e = iterator();
        while (e.hasNext()) {
            if (c.contains(e.next())) {
                modified = true;
            } else {
                e.remove();
            }
        }
        return modified;
    }

    @Override
    public void clear() {
        arr = new Object[INITIAL_SIZE];
        size = 0;
    }

    @Override
    public T remove(int index) {
        validateIndex(index);
        for (int i = index; i < arr.length - 1; i++) {
            arr[i] = arr[i + 1];
        }
        arr = Arrays.copyOf(arr, arr.length - 1);
        System.out.println(Arrays.toString(arr));
        return (T) arr;
    }

    @Override
    public int indexOf(Object o) {
        for (int i = 0; i < arr.length; i++) {
            if (o.equals(arr[i])) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        for (int i = arr.length - 1; i < arr.length; i--) {
            if (o.equals(arr[i])) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        List<T> newList = new ArrayList<>();
        int diff = toIndex - fromIndex;
        for (int i = fromIndex; i < diff + 1; i++) {
            newList.add((T) arr[i]);
        }
        return newList;
    }

    @Override
    public T get(int index) {
        validateIndex(index);
        return (T) arr[index];
    }

    @Override
    public T set(int index, T value) {
        validateIndex(index);
        arr[index] = value;
        System.out.println(Arrays.toString(arr));
        return value;
    }

    @Override
    public void add(int index, T element) {
        validateIndex(index);
        arr = Arrays.copyOf(arr, arr.length + 1);
        System.arraycopy(arr, index, arr, index + 1, arr.length - index - 1);
        System.arraycopy(arr, 0, arr, 0, index);
        arr[index] = element;
        System.out.println(Arrays.toString(arr));
        System.out.println(arr.length);
    }

    @Override
    public int size() {
        return arr.length;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        for (int i = 0; i < arr.length; i++) {
            if (o.equals(arr[i])) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new MyArrayListIterator();
    }

    class MyArrayListIterator implements Iterator<T> {
        int i = 0;

        @Override
        public boolean hasNext() {
            return i < size;
        }

        @Override
        public T next() {
            T elem = (T) arr[i];
            i++;
            return elem;
        }
    }

    @Override
    public Object[] toArray() {
        return Arrays.copyOf(arr, size);
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        T1[] arrCopy = Arrays.copyOf(a, size);
        for (int i = 0; i < arrCopy.length; i++) {
            arrCopy[i] = (T1) arr[i];
        }
        return arrCopy;
    }
}
