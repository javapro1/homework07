package myLinkedList;

import java.util.*;

public class MyLinkedList<T> implements List<T> {
    public static class Node<T> {
        @Override
        public String toString() {
            return "Node{" +
                    "value=" + value +
                    ", next=" + next +
                    ", prev=" + prev +
                    '}';
        }

        T value;
        Node<T> next;
        Node<T> prev;

        public Node(T value) {
            this.value = value;
        }
    }

    private Node<T> head;
    private Node<T> tail;
    private int size = 0;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public boolean contains(Object o) {
        for (int i = 0; i < size; i++) {
            if (get(i).equals(o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new MyLinkedListIterator();
    }

    @Override
    public Object[] toArray() {
        if (isEmpty()) {
            return null;
        }
        int i = 0;
        Object[] arr = new Object[size()];
        Node<T> node;
        for (node = head; node != null; node = node.next) {
            arr[i] = node.value;
            i++;
        }
        return arr;
    }

    @Override
    public <T1> T1[] toArray(T1[] arr) {
        arr = Arrays.copyOf(arr, size);
        int i = 0;
        Object[] result = arr;
        for (Node<T1> node = (Node<T1>) head; node != null; node = node.next) {
            result[i++] = node.value;
        }
        return arr;
    }

    @Override
    public boolean add(T t) {
        Node<T> node = new Node<>(t);
        if (isEmpty()) {
            head = tail = node;
        } else {
            tail.next = node;
            node.prev = tail;
            tail = node;
        }
        size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        Node<T> node = head;
        Node<T> x = null;
        boolean result = false;
        if (isEmpty()) {
            return false;
        }
        while (node != null) {
            if (node.value.equals(o)) {
                if (x == null) {
                    head = head.next;
                } else {
                    x.next = node.next;
                }
                result = true;
            }
            x = node;
            node = node.next;
        }
        return result;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        if (isEmpty()) {
            return false;
        }
        if (c.size() == 0) {
            return false;
        }
        if (c.size() > size()) {
            return false;
        }
        Iterator<?> it = c.iterator();
        while (it.hasNext()) {
            T value = (T) it.next();
            if (!contains(value)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        if (c == null || c.size() == 0) {
            return false;
        }
        for (T value : c) {
            add(value);
        }
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        if (c == null || c.size() == 0) {
            return false;
        }
        if (isEmpty()) {
            addAll(c);
            return true;
        }
        int i = index;
        for (T value : c) {
            add(i, value);
            i++;
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        if (c == null || c.size() == 0) {
            return false;
        }
        if (isEmpty()) {
            return false;
        }
        Iterator<?> it = c.iterator();
        while (it.hasNext()) {
            T o = (T) it.next();
            remove(o);
        }
        return true;
    }


    @Override
    public boolean retainAll(Collection<?> c) {
        if (isEmpty()) {
            return false;
        }
        if (c == null || c.size() == 0) {
            return false;
        }
        Node<T> node;
        for (node = head; node != null; node = node.next) {
            T value = node.value;
            boolean have = false;
            Iterator<?> it = c.iterator();
            while (it.hasNext()) {
                T o = (T) it.next();
                if (value.equals(o)) {
                    have = true;
                }
            }
            if (!have) {
                this.remove(value);
            }
        }
        return true;
    }

    @Override
    public void clear() {
        head = null;
        tail = null;
        size = 0;
    }

    @Override
    public T get(int index) {
        Node<T> node = getNode(index);
        return node.value;
    }

    @Override
    public T set(int index, T element) {
        Node<T> node = getNode(index);
        node.value = element;
        return element;
    }

    @Override
    public void add(int index, T element) {
        Node<T> node = getNode(index);
        Node<T> nodeElement = new Node<>(element);
        if (isEmpty()) {
            add(element);
        } else {
            nodeElement.next = node;
            nodeElement.prev = node.prev;
            node.prev.next = nodeElement;
            node.prev = nodeElement;
            size++;
        }
    }

    @Override
    public T remove(int index) {
        if (isEmpty()) {
            return null;
        }
        if (index < 0 || index > size()) {
            return null;
        }
        Node<T> node = head;
        Node<T> x = null;
        int i = -1;
        while (node != null) {
            i++;
            if (i == index) {
                if (x == null) {
                    head = head.next;
                } else {
                    x.next = node.next;
                }
                return node.value;
            }
            x = node;
            node = node.next;
        }
        return null;
    }

    @Override
    public int indexOf(Object o) {
        int i = -1;
        if (isEmpty()) {
            return -1;
        }
        Node<T> node = head;
        while (node != null) {
            i++;
            if (node.value.equals(o)) {
                return i;
            }
            node = node.next;
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        Node<T> node = head;
        int i = -1;
        int index = -1;
        while (node != null) {
            i++;
            if (node.value.equals(o)) {
                index = i;
            }
            node = node.next;
        }
        return index;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        Node<T> node = getNode(fromIndex);
        List<T> list = new ArrayList<>();
        for (int i = fromIndex; i < toIndex; i++) {
            list.add(node.value);
            node = node.next;
        }
        return list;
    }

    private class MyLinkedListIterator implements Iterator<T> {

        public MyLinkedListIterator() {
            this.cur = head;
        }

        private Node<T> cur = head;

        @Override
        public boolean hasNext() {
            return cur != null;
        }

        @Override
        public T next() {
            T value = cur.value;
            cur = cur.next;
            return value;
        }

    }

    private Node<T> getNode(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException(index);
        }
        Node<T> node = head;
        for (int i = 0; i < index; i++) {
            node = node.next;
        }
        return node;
    }
}
