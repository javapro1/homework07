package myLinkedList;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> list = new MyLinkedList<>();
        List<String> listAdd = new MyLinkedList<>();
        listAdd.add("One");
        listAdd.add("Two");
        listAdd.add("Three");
        listAdd.add("Four");
        listAdd.add("Five");
        list.add("Dima");
        list.add("Anton");
        list.add("Nata");
        list.add("Tanya");

        for (String arr: list) {
            System.out.println(arr);
        }
        System.out.println(list.get(3));
        list.add("Alex");
        System.out.println(list.size());
        list.add(4,"Denis");
        System.out.println(list.contains("Nata"));
        list.set(2,"NEWNAME");
        list.remove("Anton");
        list.remove(0);
        list.clear();
        list.addAll(listAdd);
        list.addAll(4, listAdd);
        System.out.println(list.indexOf("One"));
        System.out.println(list.lastIndexOf("Two"));
        list.removeAll(listAdd);
        list.retainAll(listAdd);
        list.addAll(listAdd);
        System.out.println(list.subList(0,3));
        for (String arr: list) {
            System.out.println(arr);
        }
    }
}
